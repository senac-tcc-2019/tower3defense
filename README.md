# Tower3Defense
Projeto de desenvolvimento de Jogo de plataforma 3D para cadeira de Projeto de Análise e Desenvolvimento de Sistemas, realizando-se na  Faculdade de Tecnologia Senac Pelotas (RS), este projeto contará com a orientação do Me. Gladimir Ceroni Catarino.

## Introdução
Tower3Defense será um jogo desenvolvido usando a Linguagem de Programação C# e utilizará de recursos de motorização da ferramenta Unity (**[Unity Technologies](https://unity.com/pt)**). Será um jogo no gênero Tower Defense em 3D.


## Testar o jogo

Baixe o executável do jogo no site: [GameJolt - Tower3Defense](https://gamejolt.com/games/tower3defense/441798).

Após realizar o download, extraia e acesse a pasta do jogo, logo após execute o arquivo Tower3Defense.exe.

## Requisitos

### Git
É necessário ter conhecimentos de alguns comandos Git, informações sobre comandos disponíveis no **[site oficial](https://about.gitlab.com/training/)**.

### Unity
O motor de jogos Unity necessita de alguns requisitos de sistema para que o jogo funcione corretamente, conforme detalhados no **[site oficial](https://unity3d.com/pt/unity/system-requirements)**.


## Clone ou Download
Clone via SSH: 

**`git@gitlab.com:senac-tcc-2019/tower3defense.git`**

Clone via HTTP: 

**`https://gitlab.com/senac-tcc-2019/tower3defense.git`**

Também é possível realizar baixar o projeto em outros formatos na opção "Download".

## Artigo

Artigo disponível no [overleaf](https://www.overleaf.com/read/cbjsghsgzbqr).

## Documentação
Documentação disponivel na **[WIKI](https://gitlab.com/senac-tcc-2019/tower3defense/wikis/home)** deste projeto.

## Licença
Este projeto usufrui da seguinte licença de uso: **[MIT](https://gitlab.com/senac-tcc-2019/tower3defense/blob/master/LICENSE)**.

